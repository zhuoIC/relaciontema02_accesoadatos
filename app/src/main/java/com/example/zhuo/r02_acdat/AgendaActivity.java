package com.example.zhuo.r02_acdat;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.zhuo.r02_acdat.utils.Memoria;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;

public class AgendaActivity extends AppCompatActivity implements View.OnClickListener{

    private Button btnSave, btnLoad, btnRemove;
    private EditText edtName, edtLastName, edtPhone, edtEmail;
    private TextView txvLoadContacts;
    static final String NOMBREFICHERO = "agenda.txt";
    private Memoria miMemoria;
    private File miFichero;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_agenda);

        txvLoadContacts = findViewById(R.id.txvLoadContacts);

        btnSave = findViewById(R.id.btnSave);
        btnSave.setOnClickListener(this);

        btnLoad = findViewById(R.id.btnLoad);
        btnLoad.setOnClickListener(this);

        btnRemove = findViewById(R.id.btnRemove);
        btnRemove.setOnClickListener(this);

        edtName = findViewById(R.id.edtName);
        edtLastName = findViewById(R.id.edtLastName);
        edtPhone = findViewById(R.id.edtPhone);
        edtEmail = findViewById(R.id.edtEmail);

        miMemoria = new Memoria(getApplicationContext());

        miFichero = new File(getApplicationContext().getFilesDir(), NOMBREFICHERO);

    }


    @Override
    public void onClick(View v) {
        if (v == btnSave){

            saveContacts(edtName.getText().toString(), edtLastName.getText().toString(), edtPhone.getText().toString(), edtEmail.getText().toString());
        }
        if (v == btnLoad){
            listarContactos();
        }
        if (v == btnRemove){
            borrarContactos();
        }
    }

    private void saveContacts(String name, String lastName, String phone, String email) {
        String texto;

        if (isFull(name, lastName, phone, email)){
            texto = name + "," + lastName + "," + phone + "," + email;
            if(miMemoria.escribirInterna(NOMBREFICHERO, texto, true, "UTF-8")){
                Toast.makeText(this, "Contacto guardado", Toast.LENGTH_SHORT).show();
            }
        }

        else{
            Toast.makeText(this, "Rellena todos los campos", Toast.LENGTH_SHORT).show();
        }
    }

    private boolean isFull(String name, String lastName, String phone, String email) {
        return !TextUtils.isEmpty(name) && !TextUtils.isEmpty(lastName) && !TextUtils.isEmpty(phone) && !TextUtils.isEmpty(email);
    }

    public void listarContactos(){

        FileInputStream fis;
        InputStreamReader isr;
        BufferedReader br;
        String linea;
        String[] datosDelContacto;

        try {
            fis = new FileInputStream(miFichero);
            isr = new InputStreamReader(fis);
            br = new BufferedReader(isr);

            txvLoadContacts.setText("");
            while ((linea = br.readLine()) != null) {
                datosDelContacto = linea.split(",");
                txvLoadContacts.setText(txvLoadContacts.getText().toString() + "Nombre: " + datosDelContacto[0] + "\n");
                txvLoadContacts.setText(txvLoadContacts.getText().toString() + "Apellidos: " + datosDelContacto[1] + "\n");
                txvLoadContacts.setText(txvLoadContacts.getText().toString() + "Teléfono: " + datosDelContacto[2] + "\n");
                txvLoadContacts.setText(txvLoadContacts.getText().toString() + "Correo: " + datosDelContacto[3] + "\n\n");
            }

            br.close();
            isr.close();
            fis.close();

        } catch (FileNotFoundException e){
            Toast.makeText(this, "La agenda aún no se ha creado", Toast.LENGTH_SHORT).show();
        } catch (IOException e) {
            Toast.makeText(this, "Error de E/S.", Toast.LENGTH_SHORT).show();
        } catch (Exception e){
            Toast.makeText(this, "Error: " + e.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    public void borrarContactos(){
        if (miFichero.exists()){
            miFichero.delete();
            txvLoadContacts.setText("");
            Toast.makeText(this, "Se han eliminado los contactos", Toast.LENGTH_SHORT).show();
        }
        else{

            Toast.makeText(this, "No hay contactos", Toast.LENGTH_SHORT).show();
        }
    }
}
