package com.example.zhuo.r02_acdat;

import android.media.MediaPlayer;
import android.os.CountDownTimer;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.zhuo.r02_acdat.utils.Memoria;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;

public class AlarmaActivity extends AppCompatActivity implements View.OnClickListener {

    private Button btnPlay, btnSave, btnRemove, btnShow;
    private EditText edtTime, edtMessage;
    private Memoria memoria;
    private File file;
    private TextView txvVerMensaje;
    private MediaPlayer mediaPlayer;
    private MiContador miContador;
    private static final int ALARMAS = 5;
    private int nAlarmas;
    private String [] mensajesAlarmas;
    private int [] tiemposAlarmas;
    private int turno;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alarma);

        btnShow = findViewById(R.id.btnVerAlarmas);
        btnShow.setOnClickListener(this);
        btnSave = findViewById(R.id.btnGuardarAlarma);
        btnSave.setOnClickListener(this);
        btnPlay = findViewById(R.id.btnLanzarAlarmas);
        btnPlay.setOnClickListener(this);
        btnRemove = findViewById(R.id.btnBorrarAlarmas);
        btnRemove.setOnClickListener(this);

        txvVerMensaje = findViewById(R.id.txvVerMensaje);
        edtMessage = findViewById(R.id.edtIntroduceMensaje);
        edtTime = findViewById(R.id.edtIntroduceTiempo);
        memoria = new Memoria(getApplicationContext());
        nAlarmas = 0;
        mensajesAlarmas = new String[ALARMAS];
        tiemposAlarmas = new int[ALARMAS];
        turno = 0;
        file = new File(Environment.getExternalStorageDirectory().getAbsolutePath(), "alarmas.txt");
        if (file.exists()) {
            comprobarAlarmas();
        }
        mediaPlayer = MediaPlayer.create(this,R.raw.alarm);
    }

    @Override
    public void onClick(View v) {
        if (v == btnSave){
            saveAlarms(edtTime.getText().toString(), edtMessage.getText().toString());
        }

        if (v == btnPlay){
            playAlarms();
        }

        if (v == btnRemove){
            removeAlarms();
        }

        if (v == btnShow){
            showAlarm();
        }
    }

    private void removeAlarms(){
        comprobarAlarmas();
        if (file.exists()) {
            file.delete();
            nAlarmas = 0;
            txvVerMensaje.setText("");
            showMessage("Se han borrado todas las alarmas");
        }
    }

    private void saveAlarms(String time, String message){
        if(memoria.disponibleEscritura()) {
            if (nAlarmas < ALARMAS) {
                if (isFull(time, message)){
                    String alarma = time + "," + message;
                    memoria.escribirExterna("alarmas.txt", alarma, true, "UTF-8");
                    comprobarAlarmas();
                    showMessage("Alarma guardada");
                }else{
                    showMessage("Alguno de los campos no ha sido rellenado");
                }
            }
            else {
                showMessage("Ya tienes 5 alarmas creadas");
            }
        }
    }

    private boolean isFull(String time, String message) {
        return !TextUtils.isEmpty(time) && !TextUtils.isEmpty(message);
    }

    private void showAlarm(){
        comprobarAlarmas();
        txvVerMensaje.setText("");
        for (int i = 0; i < nAlarmas; i++){
            txvVerMensaje.setText(txvVerMensaje.getText().toString() +
                    "Alarma: " + mensajesAlarmas[i] +
                    "\t   Tiempo: " + tiemposAlarmas[i]  + "\n");
        }
    }

    private void comprobarAlarmas(){
        nAlarmas = 0;
        FileInputStream fis;
        InputStreamReader isr;
        BufferedReader br;
        String linea;
        String [] estaAlarma;

        try {
            fis = new FileInputStream(file);
            isr = new InputStreamReader(fis);
            br = new BufferedReader(isr);

            while ((linea = br.readLine())!= null) {
                estaAlarma = linea.split(",");
                tiemposAlarmas[nAlarmas] = Integer.parseInt(estaAlarma[0]);
                mensajesAlarmas[nAlarmas++] = estaAlarma[1];
            }

            br.close();
            isr.close();
            fis.close();

        } catch (FileNotFoundException e){
            showMessage("No hay ninguna alarma creada");
        } catch (IOException e) {
            showMessage("Error de E/S.");
        } catch (Exception e){
            showMessage("Error: " + e.getMessage());
        }
    }

    private void playAlarms(){
        turno = 0;
        comprobarAlarmas();
        if (nAlarmas > 0) {
            miContador = new MiContador((long) tiemposAlarmas[turno] * 60 * 1000, (long) 1000.0);
            miContador.start();
            btnShow.setEnabled(false);
            btnRemove.setEnabled(false);
            btnPlay.setEnabled(false);
            btnSave.setEnabled(false);
        }

    }

    public class MiContador extends CountDownTimer {
        public MiContador(long startTime, long interval) {
            super(startTime, interval);
        }

        @Override
        public void onTick(long millisUntilFinished) {
            long minutos = (millisUntilFinished/1000) / 60;
            long segundos = (millisUntilFinished/1000) % 60;
            txvVerMensaje.setText("La alarma " + mensajesAlarmas[turno] + " sonará en " + minutos + ":" + segundos );
        }

        @Override
        public void onFinish()
        {
            mediaPlayer.start();
            turno++;
            if (turno < nAlarmas) {
                miContador = new MiContador((long) tiemposAlarmas[turno] * 60 * 1000, (long) 1000.0);
                miContador.start();
            }
            else{
                txvVerMensaje.setText("Todas las alarmas han finalizado");
                btnShow.setEnabled(true);
                btnRemove.setEnabled(true);
                btnPlay.setEnabled(true);
                btnSave.setEnabled(true);
            }

        }
    }

    private void showMessage(String message){
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }
}