package com.example.zhuo.r02_acdat;

import android.app.ProgressDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.FileAsyncHttpResponseHandler;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;


import cz.msebera.android.httpclient.Header;

public class ConversorActivity extends AppCompatActivity implements View.OnClickListener{

    private double getCambio;
    private EditText etxE, etxD, edtGetCambio;
    private Button btnConvertir;
    private RadioButton rbtEaD;
    private TextView txvResultado;
    private double cambio;
    boolean isDownloaded;
    private final static String URL = "http://alumno.mobi/~alumno/superior/hernandez/cambio.txt";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_conversor);
        etxE =  findViewById(R.id.etxE);
        etxD = findViewById(R.id.etxD);
        edtGetCambio = findViewById(R.id.edtGetCambio);
        btnConvertir =  findViewById(R.id.btnConvertir);
        rbtEaD = findViewById(R.id.rbtnE);
        txvResultado = findViewById(R.id.txvResultado);

        btnConvertir.setOnClickListener(this);
        descargaFichero(URL);
    }

    @Override
    public void onClick(View view)
    {

        if (view == btnConvertir) {
            try {
                if (isDownloaded) {
                    getCambio = Double.parseDouble(edtGetCambio.getText().toString());
                    if (rbtEaD.isChecked()) {
                        etxD.setText(String.format("%.2f", Double.parseDouble(etxE.getText().toString()) / getCambio));
                    } else {

                        etxE.setText(String.format("%.2f", Double.parseDouble(etxD.getText().toString()) * getCambio));
                    }
                } else {
                    Toast.makeText(this, "No se puede realizar la conversion porque no se descargó cambio.txt con exito", Toast.LENGTH_SHORT).show();
                }
            } catch (NumberFormatException e)
            {
                Toast.makeText(this, "Ha ocurrido un error en la lectura de los números", Toast.LENGTH_SHORT).show();
            }
            catch(Exception e){
                Toast.makeText(this, "Error: " + e.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }
    }


    private void descargaFichero(String url)
    {

        AsyncHttpClient client = new AsyncHttpClient();
        client.get(url, new FileAsyncHttpResponseHandler(/* Context */ this) {
            ProgressDialog pd;

            @Override
            public void onStart() {
                pd = new ProgressDialog(ConversorActivity.this);
                pd.setTitle("Por favor espere...");
                pd.setMessage("AsyncHttpResponseHadler está en progreso");
                pd.setIndeterminate(false);
                pd.setCancelable(false);
                pd.show();
                Toast.makeText(ConversorActivity.this, "Descargando cambio.txt...", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, File file) {
                Toast.makeText(ConversorActivity.this, "Se ha producido un error al descargar el fichero cambio.txt", Toast.LENGTH_SHORT).show();
                txvResultado.setText("El fichero cambio.txt no se ha descargado correctamente");
                isDownloaded = false;
            }


            @Override
            public void onSuccess(int statusCode, Header[] headers, File file) {
                Toast.makeText(ConversorActivity.this, "El fichero cambio.txt se ha descargado correctamente", Toast.LENGTH_SHORT).show();
                FileInputStream fis;
                try {
                    fis = new FileInputStream(file);
                    BufferedReader in = new BufferedReader(new InputStreamReader(fis));

                    String linea;
                    while ((linea = in.readLine()) != null) {
                        cambio = Double.parseDouble(linea);
                    }
                    in.close();
                    fis.close();
                    isDownloaded = true;
                    txvResultado.setText("El valor del fichero cambio.txt descargado es: " + String.valueOf(cambio));
                    edtGetCambio.setText(String.valueOf(cambio));
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFinish() {
                pd.dismiss();
            }

        });
    }
}
