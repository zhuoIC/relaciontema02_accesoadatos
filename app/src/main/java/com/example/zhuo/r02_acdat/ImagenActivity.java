package com.example.zhuo.r02_acdat;

import android.app.ProgressDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.jakewharton.picasso.OkHttp3Downloader;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.FileAsyncHttpResponseHandler;
import com.squareup.picasso.Picasso;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.Header;
import okhttp3.OkHttpClient;

public class ImagenActivity extends AppCompatActivity  implements  View.OnClickListener{

    private Button btnDownload, btnPrevious, btnNext;
    private ImageView imgMostrar;
    private EditText edtPath;
    private List<String> paths;
    int imgSelected;
    boolean isImagesDownloaded;
    private String URL = "http://alumno.mobi/~alumno/superior/hernandez/enlaces.txt";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_imagen);
        imgSelected = 0;
        isImagesDownloaded = false;
        btnDownload = findViewById(R.id.btnDownload);
        btnDownload.setOnClickListener(this);
        btnPrevious = findViewById(R.id.btnPrevious);
        btnPrevious.setOnClickListener(this);
        btnNext = findViewById(R.id.btnNext);
        btnNext.setOnClickListener(this);
        imgMostrar = findViewById(R.id.imgMostrar);
        edtPath = findViewById(R.id.edtPath);
        edtPath.setText(URL);
        paths = new ArrayList<>();
    }

    @Override
    public void onClick(View v) {
        if (v == btnDownload){
            descargaFichero(edtPath.getText().toString());
        }

        if (v == btnPrevious) {
            if(isImagesDownloaded)
                previousImage();
            else
                Toast.makeText(this, "No hay imagenes que cargar", Toast.LENGTH_SHORT).show();
        }

        if (v == btnNext) {
            if(isImagesDownloaded) {
                nextImage();
            }
            else {
                Toast.makeText(this, "No hay imagenes que cargar", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void descargaFichero(String url)
    {
        AsyncHttpClient client = new AsyncHttpClient();
        client.get(url, new FileAsyncHttpResponseHandler(/* Context */ this) {
            ProgressDialog pd;
            @Override
            public void onStart() {
                pd = new ProgressDialog(ImagenActivity.this);
                pd.setTitle("Please Wait..");
                pd.setMessage("AsyncHttpResponseHadler is in progress");
                pd.setIndeterminate(false);
                pd.setCancelable(false);
                pd.show();
                Toast.makeText(ImagenActivity.this, "Descargando...", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, File file) {
                Toast.makeText(ImagenActivity.this, "Se ha producido un error", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, File file) {
                Toast.makeText(ImagenActivity.this, "It just works", Toast.LENGTH_SHORT).show();
                FileInputStream fis = null;
                try {
                    fis = new FileInputStream(file);
                    BufferedReader in = new BufferedReader(new InputStreamReader(fis));

                    String aLine = null;
                    while ((aLine = in.readLine()) != null) {
                        paths.add(aLine);
                    }
                    in.close();
                    fis.close();
                    isImagesDownloaded = true;
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFinish() {
                pd.dismiss();
                if(isImagesDownloaded){
                    loadImage(paths.get(imgSelected).toString());
                }
            }

        });
    }


    private void loadImage(String ruta) {
        OkHttpClient client = new OkHttpClient();
        Picasso picasso = new Picasso.Builder(this).downloader(new OkHttp3Downloader(client)).build();
        picasso.with(this)
                .load(ruta)
                .placeholder(R.drawable.placeholder) // Carga
                .error(R.drawable.error) // Error
                .into(imgMostrar);

    }

    private void nextImage(){
            imgSelected = (imgSelected + paths.size() + 1) % paths.size();
            loadImage(paths.get(imgSelected));
    }

    private void previousImage(){
            imgSelected = (imgSelected + paths.size() - 1) % paths.size();
            loadImage(paths.get(imgSelected));
    }

}