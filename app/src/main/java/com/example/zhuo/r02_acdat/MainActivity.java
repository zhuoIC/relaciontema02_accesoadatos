package com.example.zhuo.r02_acdat;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{

    private Button btnAgenda, btnAlarma, btnCalendario, btnWeb, btnImagen, btnConversor, btnSubida;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnAgenda = findViewById(R.id.btnAgenda);
        btnAlarma =  findViewById(R.id.btnAlarma);
        btnCalendario = findViewById(R.id.btnCalendario);
        btnWeb = findViewById(R.id.btnWeb);
        btnImagen = findViewById(R.id.btnImagen);
        btnConversor = findViewById(R.id.btnConversor);
        btnSubida = findViewById(R.id.btnSubida);

        btnAgenda.setOnClickListener(this);
        btnAlarma.setOnClickListener(this);
        btnCalendario.setOnClickListener(this);
        btnWeb.setOnClickListener(this);
        btnImagen.setOnClickListener(this);
        btnConversor.setOnClickListener(this);
        btnSubida.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {

        if (view == btnAgenda){
            startActivity( new Intent(this,AgendaActivity.class));
        }
        if (view == btnAlarma){
            startActivity( new Intent(this,AlarmaActivity.class));
        }
        if (view == btnCalendario){
            startActivity( new Intent(this,CalendarioActivity.class));
        }
        if (view == btnWeb){
            startActivity( new Intent(this,WebActivity.class));
        }
        if (view == btnImagen){
            startActivity( new Intent(this,ImagenActivity.class));
        }
        if (view == btnConversor){
            startActivity( new Intent(this,ConversorActivity.class));
        }
        if (view == btnSubida){
            startActivity( new Intent(this,SubidaActivity.class));
        }
    }
}
